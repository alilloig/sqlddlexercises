DROP DATABASE IF EXISTS departamento;

CREATE DATABASE departamento;
USE departamento;

CREATE TABLE Profesores(
	DNI CHAR(10) PRIMARY KEY,
	nombre VARCHAR (20) NOT NULL,
	apellidos VARCHAR(30) NOT NULL,
	cuerpoDocente VARCHAR(20),
	departamento int
);

CREATE TABLE Departamentos (
	cod int PRIMARY KEY AUTO_INCREMENT,
	nombre VARCHAR(20) NOT NULL,
	area VARCHAR(20) NOT NULL,
	director CHAR(10),
	fechaDireccion DATE
);

CREATE TABLE Alumnos (
	DNI CHAR(10) PRIMARY KEY,
	nombre VARCHAR (20) NOT NULL,
	apellidos VARCHAR(30) NOT NULL,
	calle VARCHAR(20),
	numero INT,
	ciudad VARCHAR(20),
	cp CHAR(5)
);

CREATE TABLE Asignaturas(
	cod int PRIMARY KEY AUTO_INCREMENT,
	nombre VARCHAR(20) NOT NULL,
	creditos INT,
	descripcion VARCHAR(50),
	cuatrimestres VARCHAR(10),
	departamento INT,
	profesor CHAR(10)
);

CREATE TABLE Titulaciones(
	cod int PRIMARY KEY AUTO_INCREMENT,
	nombre VARCHAR(30) NOT NULL,
	creditos INT,
	cuatrimestre VARCHAR(10)
);

ALTER TABLE Profesores ADD CONSTRAINT FK_departprof FOREIGN KEY (departamento) REFERENCES Departamentos(cod);
ALTER TABLE Departamentos ADD CONSTRAINT FK_direct FOREIGN KEY (director) REFERENCES Profesores(DNI);
ALTER TABLE Asignaturas ADD CONSTRAINT FK_departasig FOREIGN KEY (departamento) REFERENCES Departamentos(cod);
ALTER TABLE Asignaturas ADD CONSTRAINT FK_prof FOREIGN KEY (profesor) REFERENCES Profesores(DNI);


CREATE TABLE TelefonosAlumnos(
	ID INT,
	alumno CHAR(10),
	numero VARCHAR(20) NOT NULL,
	descripcion ENUM('fijo','movil'),
	CONSTRAINT PK_telef PRIMARY KEY (ID, alumno)
);

CREATE TABLE TitulacionesOfertarAsignaturas(
	asignatura INT REFERENCES Asignaturas(cod),
	titulacion INT REFERENCES Titulaciones(cod),
	CONSTRAINT PK_titasign PRIMARY KEY (asignatura, titulacion)

);
CREATE TABLE AlumnosMatricularseAsgignaturas(
	alumno CHAR(10) REFERENCES Alumnos(DNI),
	asignatura INT REFERENCES Asignaturas(cod),
	CONSTRAINT PK_alummatasig PRIMARY KEY (alumno, asignatura)
);
CREATE TABLE ProfesoresImpartirAsignaturas(
	profesor CHAR(10) REFERENCES Profesores(DNI),
	asignatura INT REFERENCES Asignaturas(cod),
	CONSTRAINT PK_profasign PRIMARY KEY (profesor, asignatura)
);
CREATE TABLE AlumnosExaminarseAsignaturas(
	alumno CHAR(10) REFERENCES Alumnos(DNI),
	asignatura INT REFERENCES Asignaturas(cod),
	CONSTRAINT PK_alumexasig PRIMARY KEY (alumno, asignatura)
);